/*
* Hackerrank Problem details -> 
*   https://www.hackerrank.com/contests/w29/challenges/day-of-the-programmer/problem
*/
object DayOfTheProgrammer {

    val switchYear = "26.09.1918"
    val nonLeapYear = "13.09."
    val leapYear = "12.09."
        
    def main(args: Array[String]) {
        val year = readInt
        println(getProgrammersDay(year))
    }
    
    def getProgrammersDay(year: Int): String = {
        if(year == 1918) {
            switchYear
        } else if (year < 1918) {
            s"${getAns(checkJulian(year))}$year"
        } else {
            s"${getAns(checkGregorian(year))}$year"
        } 
    }
    
    def checkJulian(year: Int): Boolean = year % 4 == 0
    
    def checkGregorian(year: Int): Boolean = {
        val isDivisibleBy4 = year%4==0
        val isDivisibleBy100 = year%100==0
        val isDivisibleBy400 = year%400==0
        
        isDivisibleBy400 || (isDivisibleBy4 && !isDivisibleBy100)
    }
    
    def getAns(isLeap: Boolean): String = {
        if(isLeap) {
            leapYear
        } else {
            nonLeapYear
        }
    }
}
