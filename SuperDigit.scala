import scala.annotation.tailrec

/*
* Hackerrank Problem details -> 
*   https://www.hackerrank.com/challenges/super-digit/problem
*/

object SuperDigit {

    def main(args: Array[String]) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution
*/
        val sc = new java.util.Scanner (System.in)
        val n = sc.next().toList.map(_.toInt - '0') //get input as String, constraint is up to 10^100000
        val k = sc.nextInt()
        
        println(solve(n, k))
    }
    
    def parseIntToDigitList(number: Long): List[Int] = {
        number.toString.toList.map(_.toInt - '0') //subtract '0' (48) ascii to get equivalent in Integer 
    }
    
    def solve(n: List[Int], k: Int): Int = {
        
        @tailrec
        def solveHelper(num: List[Int]): List[Int] = {
            if(num.size == 1) {
                num
            } else {
                val newNum = parseIntToDigitList(num.sum)
                solveHelper(newNum)
            }
        }
        
        val firstAns = solveHelper(n) //simplify first superDigit sum
        solveHelper(List.fill(k)(firstAns.head)).head    //multiply it k times and recompute superDigit
    }
    
}