import  scala.collection.mutable.HashMap

/*
* Hackerrank Problem details -> 
*   https://www.hackerrank.com/challenges/ctci-coin-change/problem
*/
object DPCoinChange {

    def main(args: Array[String]) {
        val sc = new java.util.Scanner (System.in);
        val n = sc.nextInt();
        val coinsType = sc.nextInt();
        val coins = new Array[Int](coinsType);
        for(coins_i <- 0 to coinsType-1) {
           coins(coins_i) = sc.nextInt();
        }
        
        println(getChange(n, coins.toList))
    }
    
    def getChange(n: Int, coinList: List[Int]): Long = {
        
        //memoization
        val lookUp: HashMap[(Int, List[Int]), Long] = HashMap()
        
        def computeChange(currentValue: Int, coinsRemaining: List[Int]): Long = {
            lookUp.get((currentValue, coinsRemaining)) match {
                case Some(value) => value // value already in the lookup table
                case None =>
                     if(currentValue == 0){ //change found
                          1L
                     } else if (currentValue < 0) { //change not possible
                          0L
                     } else if (coinsRemaining.isEmpty){ //no more coins to get change
                          0L
                     } else {
                          //compute change with and without head
                          val withoutHead = computeChange(currentValue, coinsRemaining.tail)
                          val withHead = computeChange(currentValue-coinsRemaining.head, coinsRemaining)

                          //store in lookup for future computation
                          lookUp.put((currentValue, coinsRemaining), withoutHead+withHead)

                          withoutHead + withHead
                     }
            }
             
               
        }
        
        computeChange(n, coinList)
        
    }
}
