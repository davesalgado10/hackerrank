/*
* Hackerrank Problem details -> 
*   https://www.hackerrank.com/challenges/unbounded-knapsack/problem
*/
object Knapsack {

    def main(args: Array[String]) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution
*/
        val t = readInt()
        for(i <- 1 to t){
            val line1 = readLine().split(" ").toList.map(_.toInt)
            val n = line1(0)
            val targetWeight = line1(1)
            val itemList = readLine().split(" ").toList.map(_.toInt)
            
            println(knapsack(itemList, targetWeight))
        }
    }
    
    def knapsack(itemList: List[Int], targetWeight: Int): Int = {
        def knapsackHelper(currentWeight: Int, items: List[Int] ): Int = {
           if(items.isEmpty){
                 currentWeight  
           } else if(currentWeight + items.head > targetWeight) { 
               knapsackHelper(currentWeight, items.tail) //head is greater than allowed item in knapsack
           } else {
               //check which will be nearer to target weight, if head is included or not
               val newWeight = currentWeight + items.head 
               val headIncluded = knapsackHelper(newWeight, items) 
               val headRemoved = knapsackHelper(currentWeight, items.tail)
               Math.max(headIncluded, headRemoved)
           }
        }
        
        knapsackHelper(0, itemList)
    }
    
    
}