import scala.annotation.tailrec

/*
* Hackerrank Problem details -> 
*   https://www.hackerrank.com/challenges/string-reductions/problem
*/
object StringReduction {

    def main(args: Array[String]) {
        val str = readLine()    
        println(compress(str))
    }
    
    def compress(str: String): String = {
        
        /*
        * Algo:
        *  Traverse the input String
        *  Use a set to determine if character has occured before. 
        *    include character to ouput if it's not yet seen (not yet in lookup set)
        *    disregard it if it's already seen (found in lookup set)
        */
        @tailrec
        def compressHelper(currStr: String, output: StringBuilder, lookUpSet: Set[Char]): String = {
            if(currStr.isEmpty) {
                output.toString
            } else if(lookUpSet.contains(currStr.head)) {
                compressHelper(currStr.tail, output, lookUpSet)
            } else {
                compressHelper(currStr.tail, output.append(currStr.head), lookUpSet + currStr.head)
            }
                
        }
        
        compressHelper(str, new StringBuilder(""), Set[Char]())
        
    }
    
}