import scala.math.BigInt
import scala.annotation.tailrec

/*
* Hackerrank Problem details -> 
*   https://www.hackerrank.com/challenges/fibonacci-modified/problem
*/
   
object FibonacciModified {

    def main(args: Array[String]) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution
*/
        val sc = new java.util.Scanner (System.in);
        
        println(modifiedFibo(sc.nextInt, sc.nextInt, sc.nextInt))    
    }
    
    def modifiedFibo(first: BigInt, second: BigInt, target: Int): BigInt = {
        
        @tailrec
        def fiboHelper(current: BigInt, next: BigInt, counter:Int ): BigInt = {
            if(counter == target){
                current
            } else {
                fiboHelper(next, current + next.pow(2), counter + 1)
            }
        }
        
        fiboHelper(first, second, 1)
    }
}