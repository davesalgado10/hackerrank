import  scala.collection.mutable.HashMap

/*
* Hackerrank Problem details -> 
*   https://www.hackerrank.com/challenges/functional-programming-the-sums-of-powers/problem
*/
object SumOfPowers {
            
    def getNPowersBelowX(X: Int, N: Int): List[Int] = {
        
        def getNPowersHelper(acc: List[Int], current: Int): List[Int] = {
            val currPow = Math.pow(current, N).toInt 
            if(X < currPow){
                acc
            } else {
                getNPowersHelper(currPow :: acc, current+1 )    
            }
        }
        
        getNPowersHelper(Nil, 1)
    }
    
    /*
        Find the number of ways that a given integer, X, can be expressed as the sum of the Nth power of unique, natural numbers.
    */
    def numberOfWays(X:Int, N:Int):Int = {
        val candidatePowerList = getNPowersBelowX(X,N) //generate all i^N below X
        val lookUp: HashMap[(Int, List[Int]), Int] = HashMap()       
        
        def solve(acc: Int, powerList: List[Int]): Int = {
            lookUp.get((acc, powerList)) match {
                case Some(value) => value //found computed value in lookup
                case None =>
                    if(acc == X){
                        1 //found a sum of powers
                    } else if(acc > X){
                        0 //exceeded target
                    } else if (powerList.isEmpty){
                        0 //not possible to get X
                    } else{
                        val withoutHead = solve(acc, powerList.tail)
                        val addHead = solve(acc + powerList.head, powerList.tail)
                        
                        val ans = withoutHead + addHead
                        lookUp.put((acc, powerList), ans)
                        ans
                    }
            }   
        }
        
        solve(0, candidatePowerList)
    }
    
    

    def main(args: Array[String]) {
       println(numberOfWays(readInt(),readInt()))
    }
}
