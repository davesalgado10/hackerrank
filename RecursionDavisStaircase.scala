import scala.collection.mutable.HashMap

/*
* Hackerrank Problem details -> 
*   https://www.hackerrank.com/challenges/ctci-recursive-staircase/problem
*/
    
object RecursionDavisStairCase {

    def main(args: Array[String]) {
        val t = readInt()
        for(i <- 1 to t) {
            val totalStair = readInt()
            println(solve(totalStair))
        }
    }
    
    def solve(totalStair: Int): Int = {
        
        val memoi: HashMap[Int, Int] = HashMap()
        
        def solveHelper(currentStep: Int): Int  = {
            memoi.get(currentStep) match{
                case Some(stored) => stored //stored in lookup
                case None =>
                    if(currentStep > totalStair){ //target stair steps exceeded
                        0
                    }
                    else if (currentStep == totalStair){ //targe reached
                        1
                    }
                    else {
                        //try every possible move (1 step, 2 steps and 3 steps) from currentStep
                        val ans = solveHelper(currentStep + 1) +
                        solveHelper(currentStep + 2) +
                        solveHelper(currentStep + 3)

                        memoi.put(currentStep, ans) //store in lookup for future computation
                        ans
                    }
            }
            
            
        }
        
        solveHelper(0)
        
    }
}
