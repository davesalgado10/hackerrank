import scala.annotation.tailrec

/*
* Hackerrank Problem details -> 
*   https://www.hackerrank.com/challenges/queens-attack-2/problem
*/
object QueensAttackII {

    def main(args: Array[String]) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution
*/
        val sc = new java.util.Scanner (System.in)
        val n = sc.nextInt()
        val k = sc.nextInt()
        val rQueen = sc.nextInt()
        val cQueen = sc.nextInt()
        
        val queen = Pos(rQueen, cQueen)
        
        val setObs = (1 to k).foldLeft[Set[Pos]](Set()){
            (acc, head) => {
                 val rObstacle = sc.nextInt()
                 val cObstacle = sc.nextInt()
            
                 val obsPos = Pos(rObstacle,cObstacle)
                 acc + obsPos
            }
        }
        
        println(countQueenMoves(queen, setObs, n))
    }
    
    /*
    * Algo:
    * Calculate the distance from the Queen to an Obstacle or End of Board in all directions
    */
    def countQueenMoves(queen: Pos, obstacle: Set[Pos], size: Int): Int = {
        countUpward(queen, obstacle, size) +
        countDownward(queen, obstacle, size) +
        countLeft(queen, obstacle, size) +
        countRight(queen, obstacle, size) +
        countUpLeft(queen, obstacle, size) +
        countUpRight(queen, obstacle, size) +
        countDownLeft(queen, obstacle, size) +
        countDownRight(queen, obstacle, size)    
         
    }
    
    def countUpward(queen: Pos, obstacle: Set[Pos], size: Int): Int = {
        val upObstacle = obstacle.filter(p => p.col == queen.col && queen.row < p.row)
        if(upObstacle.isEmpty){
            size-queen.row
        } else {
            upObstacle.minBy(_.row).row - queen.row - 1
        }
    }
    
    def countDownward(queen: Pos, obstacle: Set[Pos], size: Int): Int = {
        val downObstacle = obstacle.filter(p => p.col == queen.col && queen.row > p.row)
        if(downObstacle.isEmpty){
            queen.row-1
        } else {
            queen.row - downObstacle.maxBy(_.row).row - 1 
        }
    }
    
    
    def countRight(queen: Pos, obstacle: Set[Pos], size: Int): Int = {
        val rightObstacle = obstacle.filter(p => p.row == queen.row && queen.col > p.col)
        if(rightObstacle.isEmpty){
            queen.col - 1
        } else {
           queen.col - rightObstacle.maxBy(_.col).col - 1 
        }
    }
    
    def countLeft(queen: Pos, obstacle: Set[Pos], size: Int): Int = {
        val leftObstacle = obstacle.filter(p => p.row == queen.row && queen.col < p.col)
        if(leftObstacle.isEmpty){
            size-queen.col
        } else {
            leftObstacle.minBy(_.col).col - queen.col - 1
        }
    }
    
    def countUpLeft(queen: Pos, obstacle: Set[Pos], size: Int): Int = {
        
        @tailrec
        def helper(newPos: Pos, count: Int): Int ={
            if(obstacle.contains(newPos))
                count
            else if(newPos.row > size || newPos.col < 1)
                count
            else
                helper(Pos(newPos.row+1, newPos.col -1), count+1)
        }
        
        helper(Pos(queen.row+1,queen.col-1), 0)
    }
    
    def countUpRight(queen: Pos, obstacle: Set[Pos], size: Int): Int = {
        
        @tailrec
        def helper(newPos: Pos, count: Int): Int ={
            if(obstacle.contains(newPos))
                count
            else if(newPos.row > size || newPos.col > size)
                count
            else
                helper(Pos(newPos.row+1, newPos.col +1), count+1)
        }
        
        helper(Pos(queen.row+1,queen.col+1), 0)
    }
    
    def countDownLeft(queen: Pos, obstacle: Set[Pos], size: Int): Int = {
        
        @tailrec
        def helper(newPos: Pos, count: Int): Int ={
            if(obstacle.contains(newPos))
                count
            else if(newPos.row < 1 || newPos.col < 1)
                count
            else
                helper(Pos(newPos.row-1, newPos.col -1), count+1)
        }
        
        helper(Pos(queen.row-1,queen.col-1), 0)
    }
    
    def countDownRight(queen: Pos, obstacle: Set[Pos], size: Int): Int = {
        
        @tailrec
        def helper(newPos: Pos, count: Int): Int ={
            if(obstacle.contains(newPos))
                count
            else if(newPos.row < 1 || newPos.col > size)
                count
            else
                helper(Pos(newPos.row-1, newPos.col+1), count+1)
        }
        
        helper(Pos(queen.row-1,queen.col+1), 0)
    }
    
}

case class Pos(row: Int, col: Int)