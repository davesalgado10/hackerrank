import scala.collection.immutable._
import scala.math.Ordering
import scala.io.StdIn
import scala.annotation.tailrec

/*
* Hackerrank Problem details -> 
*   https://www.hackerrank.com/challenges/minimum-average-waiting-time/problem
*/

object MinimumAverageWaitingTime {

    def main(args: Array[String]) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution
*/
        val sc = new java.util.Scanner (System.in)
        
        val n = sc.nextInt()

        val orderedSet = (1 to n).foldLeft[TreeSet[Order]](new TreeSet[Order]()(Ordering.by(order => (order.arrival, order.pizza, order.duplicateAllow)))){(acc, head) => 
            acc + Order(sc.nextLong(), sc.nextLong(), head)
        }    
        
        println(solve(orderedSet, n))
    }
    
    def solve(orderedSet: TreeSet[Order], n: Int): Long = {
     
        /**
         * Previous solution (slow)
         * Get order with minimum pizza time where arrival time is less than the previous pizza finished time (newTime)
         */
        @tailrec
        def getMinPizzaTimeUnderNewTime(remaining: TreeSet[Order], newTime: Long, minPizza: Option[Order]): Option[Order] = {
            if(remaining.isEmpty) {
                minPizza
            } else if (remaining.head.arrival > newTime) {
                minPizza
            } else {
                val newPizza = remaining.head
                val nextList = remaining.tail
                minPizza match {
                    case None =>
                        getMinPizzaTimeUnderNewTime(nextList, newTime, Option(newPizza))
                    case Some(currentMin) =>
                        if(newPizza.pizza < currentMin.pizza){
                            getMinPizzaTimeUnderNewTime(nextList, newTime, Option(newPizza))
                        } else {
                            getMinPizzaTimeUnderNewTime(nextList, newTime, minPizza)
                        }
                }
            }
        }
        
        /**
         * Current solution (better performance)
         * Maintain a list of orders sorted by pizzaTime where arrival times are less than prev pizza finished time (newTime)
         */
        @tailrec
        def getSortedByPizzaUnderTime(sortedByPizza: TreeSet[Order], toProcess: TreeSet[Order], newTime: Long): (TreeSet[Order], TreeSet[Order]) = {
            if(toProcess.isEmpty) {
                (sortedByPizza, toProcess)
            } else if (toProcess.head.arrival > newTime){
                (sortedByPizza, toProcess)
            } else{
                val newPizza = toProcess.head
                val newSortedByPizza = sortedByPizza + newPizza
                getSortedByPizzaUnderTime(newSortedByPizza, toProcess.tail, newTime)
            }
        }
        
        @tailrec
        def solveHelper(sortedByPizzaUnderTime: TreeSet[Order], toProcess: TreeSet[Order], acc: Long, currentTime: Long): Long = {
            if(toProcess.isEmpty && sortedByPizzaUnderTime.isEmpty){
                acc/n
            } else{
                //val order = getMinPizzaTimeUnderNewTime(toProcess, currentTime, None) 
                val (newSortedByPizza, newToProcess) = getSortedByPizzaUnderTime(sortedByPizzaUnderTime, toProcess, currentTime)
    
                newSortedByPizza.headOption match {
                    case None => //No order arrived while cooking prevPizza
                        val newOrder = newToProcess.head
                        val newTime = newOrder.arrival + newOrder.pizza
                        val waitTime = newOrder.pizza
                        val removedMinList = newToProcess.tail //remove pizza from the orderedSet
                        solveHelper(newSortedByPizza, removedMinList, acc + waitTime, newTime)
                    case Some(minPizzaOrder) =>
                        val newTime = currentTime + minPizzaOrder.pizza
                        val waitTime = newTime - minPizzaOrder.arrival
                        val removedMinList = newSortedByPizza - minPizzaOrder //remove pizza from the sortedByPizzaSet
                        solveHelper(removedMinList, newToProcess, acc + waitTime, newTime) 
                }
            }
        }
        
        val sortedByPizza = new TreeSet[Order]()(Ordering.by(order => (order.pizza, order.arrival, order.duplicateAllow)))
        solveHelper(sortedByPizza, orderedSet, 0L, 0L)
    }
}

case class Order(arrival: Long, pizza: Long, duplicateAllow: Int)
